﻿module Config

open FSharp.Json
open System.IO

    type Response =
        { inputs:string[];
         output:string; }
    
    type Command =
        { command:string;
            description:string;
            triggers:string[];
            imageTag:string option;
            action:string option;
            responses:Response[] option;
            randomResponses:string[] option;
            extraData:Map<string, string> option; }
    
    type ShockBotConfig =
        { commands:Command[]; }
    
    let readFile fileName = 
        File.ReadAllText(fileName) |> Json.deserialize<ShockBotConfig>
