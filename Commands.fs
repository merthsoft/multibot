﻿module Commands

open System.Linq
open Config
    type ResponseCommand = 
        { name:string;
          description:string;
          extraData:Map<string, string> option;

          trigger:string;
          parameters:string[]; 

          responses:Response[];
          randomResponses:string[] option; }
        member this.getResponse =
            let numArgs = this.parameters.Count()
            let closestMatch = match this.responses |> Array.tryFind(fun r -> r.inputs.Count() = numArgs) with
                               | Some c -> c
                               | None -> this.responses.ElementAt(this.responses |> Seq.findIndexBack(fun (r:Response) -> r.inputs.Count() <= numArgs))
            closestMatch.output
    
    type ImageCommand =
        { name:string;
          description:string;
          extraData:Map<string, string> option;
          
          trigger:string;
          parameters:string[];

          imageTag:string; }
    
    type ActionCommand =
        { name:string;
          description:string;
          extraData:Map<string, string> option;
          
          trigger:string;
          parameters:string[];
          
          action:string; }
          
    
    type ProcessedCommand =
        | Response of ResponseCommand
        | Image of ImageCommand
        | Action of ActionCommand
        | Misconfigured of string*string[]
        | NotFound of string*string[]
    
    let parseMessage (messageText:string) =
        let split = messageText.Split(" ")
        (split.[0], split.Skip(1).ToArray())

    let hasTrigger trigger (c:Command) =
        c.triggers |> Seq.contains(trigger)

    let orderResponses =
        Array.sortBy(fun (x:Response) -> x.inputs.Count())

    let findCommand (config:ShockBotConfig) (messageText:string) : ProcessedCommand =
        let (trigger, parameters) = messageText |> parseMessage
        let triggerSearch = Seq.tryFind(trigger |> hasTrigger)
        match config.commands |> triggerSearch with
        | Some c -> match (c.responses, c.imageTag, c.action) with
                    | (Some r, _, _) ->
                        Response { name = c.command; 
                                   description = c.description; 
                                   extraData = c.extraData; 
                                   trigger = trigger;
                                   parameters = parameters;

                                   responses = r |> orderResponses
                                   randomResponses = c.randomResponses }
                    | (_, Some i, _) -> 
                        Image { name = c.command; 
                                description = c.description; 
                                extraData = c.extraData; 
                                trigger = trigger;
                                parameters = parameters;
                                
                                imageTag = i; }
                    | (_, _, Some a) -> 
                        Action { name = c.command; 
                                 description = c.description; 
                                 extraData = c.extraData; 
                                 trigger = trigger;
                                 parameters = parameters;
                                 
                                 action = a; }
                    | _ -> Misconfigured (trigger, parameters)
        | None -> NotFound (trigger, parameters)
