﻿open System;
open Commands;

[<EntryPoint>]
let main argv =
    let config = "config.json" |> Config.readFile
    let commandSearch messageText = messageText |> Commands.findCommand config 
    while true do
        printf "> "
        let text = Console.ReadLine()
        let command = commandSearch text
        match command with
        | Response r -> printfn "%s" r.getResponse
        | Image i -> printfn "%s" i.imageTag
        | Action a -> printfn "%s" a.action
        | Misconfigured (trigger, _) -> printfn "Error processing %s" trigger
        | NotFound (trigger, _) -> printfn "Command not found: %s" trigger
    0
